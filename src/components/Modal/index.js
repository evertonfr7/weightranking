import React, { useContext } from 'react';
import Button from '../../components/Button';
import './index.css';

import PersonsContext from '../../contexts/persons';

function Modal() {

    const { addPerson } = useContext(PersonsContext);

    function close() {
        document.querySelector('.overlay').classList.remove('opened');
    }

    function addNewPerson(name, initialWeight, endWeight){
        addPerson(name, initialWeight, endWeight);
        document.querySelector('.modal input.name').value = '';
        document.querySelector('.modal input.initial-weight').value = '';
        document.querySelector('.modal input.end-weight').value = '';
        close();
    }
    
  
    return (
        <div className="overlay ">
            <div className="modal">
                <div className="close">✖</div>
                <input type="text" className="name" placeholder="Nome"/>
                <input type="number" className="initial-weight" placeholder="Peso Inicial" />
                <input type="number" className="end-weight" placeholder="Peso Final" />
                <Button text="OK" onClick={ () => { addNewPerson(document.querySelector('.modal input.name').value, document.querySelector('.modal input.initial-weight').value, document.querySelector('.modal input.end-weight').value) }} />
            </div>
        </div>
    );
}

export default Modal;