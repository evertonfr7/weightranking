import React from 'react';

import './index.css';

function Button(props) {

  const { onClick } = props;

  return (
    <div onClick={ onClick } className="btn" alt={ props.text} title={props.text} >{props.text}</div>
  );
}

export default Button;