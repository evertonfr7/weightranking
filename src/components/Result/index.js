import React from 'react';

import './index.css';

function Result(props) {
  let cl = '';
  let prize = '';
  if(props.type > 0){
    cl = 'positive'
  }else if(props.type === 0){
    cl = ''
  }else{
    cl = 'negative'
  }


  if(props.percent <= -10){
    prize = 'R$ 300'
  }

  if(props.percent > -10 && props.percent <= -7){
    prize = 'R$ 200'
  }

  return (
      <div className={`result ${cl}`}>
        <div className="place">#{ props.place }</div>
        <div className="name">{ props.name }</div>
        <div className="percent">{ props.percent.toFixed(3) }%</div>
        <div className="weight">{ props.weight.toFixed(2) }kg</div>
        <div className="prize">{ prize || '-' }</div>
      </div>
  );
}

export default Result;