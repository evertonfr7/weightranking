import React, { useState, useContext } from 'react';
import './App.css';
import Button from './components/Button';
import AddPerson from './components/AddPerson';
import Modal from './components/Modal';
import Podium from './components/Podium';
import Result from './components/Result';
import Person from './components/Person';
import useWindowSize from 'react-use-window-size';
import Confetti from 'react-confetti';
import PersonsContext from './contexts/persons';

function App() {


  const { width, height } = useWindowSize();

  const { persons } = useContext(PersonsContext);   
  
  const [ results, setResults ] = useState([]);  
  

  const [fallConfetti, setFallConfetti] = useState(false);

  function seeResults(){    
    setFallConfetti(true);
    document.querySelector('#root').classList.add('results-mode');
    const t = persons;
    calculate(t);
  }

  function compareNumbers(a, b) {
    return a.percent - b.percent;
  }


  function calculate(t){
    let data = [];
    t.forEach(person => {
      let p = {
        type: parseFloat(person.initialWeight) - parseFloat(person.endWeight),
        name: person.name,
        percent: (100 * (parseFloat(person.initialWeight) - parseFloat(person.endWeight))) / parseFloat(person.initialWeight) * -1,
        weight: (parseFloat(person.initialWeight) - parseFloat(person.endWeight)) * -1
      }

      data.push(p);
    });
    data.sort(compareNumbers);
    setResults(data);
  }



  return (
    <>
        { persons.length > 0 && <div className="person-wrapper">
          {persons.map(person => (
            <Person name={ person.name } initial={ person.initialWeight } final={ person.endWeight }></Person>
          ))} 
        </div> }
        <AddPerson />
        <div className="see-results-wrapper">
          <Button text="Ver Resultados" onClick={ () => { seeResults(); } }/>
        </div>
        <Modal />

        <Confetti
          width={width}
          height={height * 1.5}
          run={ fallConfetti }
        />

        { results.length >= 3 &&  <Podium st={ results[0].name } nd={results[1].name} rd={ results[2].name} /> }

        { results && <div className="results-wrapper">
          {results.map((result, i) => (
            <Result place={ i+1 } type={ result.type } name={ result.name } percent={ result.percent } weight={ result.weight }/>
          ))}
        </div>
        }
    </>
  );
}

export default App;
