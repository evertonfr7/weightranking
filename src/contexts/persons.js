import React, { createContext, useState } from 'react';
const PersonsContext = createContext({});

export function PersonsProvider({ children }) {

    const [ persons, setPersons ] = useState([]);
    function addPerson(name, initialWeight, endWeight){

        const person = {
            name,
            initialWeight,
            endWeight
        }
        setPersons([...persons, person]);
        

    }

    function removePerson(name){
        let newP = persons.filter(e => e.name !== name);
        console.log(newP);
        setPersons(newP);
    }
    return (
        
        <PersonsContext.Provider value={{ persons, addPerson, removePerson }}>
            { children }
        </PersonsContext.Provider>
    );
}

export default PersonsContext;