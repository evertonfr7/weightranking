import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { PersonsProvider } from './contexts/persons';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <PersonsProvider>
      <App />
    </PersonsProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
